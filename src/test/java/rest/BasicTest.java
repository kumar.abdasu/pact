package rest;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.authentication.FormAuthConfig;
import io.restassured.filter.session.SessionFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class BasicTest {
	
	@BeforeClass
	public void init() {
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = 8080;
		RestAssured.basePath = "/student";
	}
	
	
	@Test
	public void test1() {
		given()
		.when()
			.delete("10")
		.then()
			.log().status()
			.statusCode(200);
		
	}
	
	@Test
	public void test2() {
		given()
		.when()
			.get("/2")
		.then()
			.statusCode(200);
		
	}
	
	@Test
	public void test3() {
		given()
		.when()
			.get("/list")
		.then()
			.statusCode(200);
		
	}
	
//	@Test
	public void test4() {
		given()
			.header("key1", "val1")
			.log().params()
			.log().headers()
		.when()
			.get("/4")
		.then()
			.body("courses", hasItems("Algorithms", "Ethics"))
			.statusCode(200);		
	}
	
//	@Test
	public void test5() {
		List<String> t = get("/?programme=Medicine").path("courses");
		System.out.println(t.size());
	}
	
	//@Test
	public void test6() {
		SessionFilter f1 = new SessionFilter();
		given().auth().form("user", "user", new FormAuthConfig("/login", "uname", "pwd"))
		.filter(f1)
		.get();
		
		given().sessionId(f1.getSessionId()).when().get("/list").then().log().all();
	}
	
	@Test
	public void test7() {
		given()
			.contentType(ContentType.JSON)
			.body("{\"email\":\"sattipma@gmail.com\"}")
		.when()
			.patch("/3")
		.then()
			.log().all();
		
		given()
		.when()
			.get("/3")
		.then()
			.assertThat().body("email", equalTo("satish@gmail.com"));
	}

}
