package rest;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;

import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;

import static io.restassured.path.json.JsonPath.from;
import io.restassured.response.Response;

public class DataManipulation {

	@BeforeClass
	public void init() {
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = 8080;
		RestAssured.basePath = "/student";
	}
	
	//@Test
	public void getFirstNameOfSingleStudent() {
		given()
		.when()
			.get("/1")
		.then()
			.body("firstName", equalTo("SATISH"));
		
//		System.out.println(fName);
	}
	
	//@Test
	public void getFirstNamesOfAllStudents() {
//		ArrayList<String> fNames = 
		given()
		.when()
			.get("/list")
		.then()
			.body("firstName", hasItems("Roth", "Satish"));
		
//		boolean flag = false;
//		for (String s1 : fNames) {
//			if(s1.equalsIgnoreCase("ROTH")) {
//				flag=true;
//				break;
//			}
//		}
//		
//		if(flag) 
//			System.out.println("PASS : Record Exists");
//		else
//			System.out.println("FAIL : Record not available");
//		
//		System.out.println(fNames);
	}
	
//	@Test
	public void testCreateRecord() {
		String myBody = "{\r\n" + 
				"    \"firstName\": \"Satish\",\r\n" + 
				"    \"lastName\": \"T\",\r\n" + 
				"    \"email\": \"satish123@gmail.com\",\r\n" + 
				"    \"programme\": \"Automation\",\r\n" + 
				"    \"courses\": [\r\n" + 
				"        \"Selenium\",\r\n" + 
				"        \"RestAssured\"\r\n" + 
				"    ]\r\n" + 
				"}";
		
		given()
			.contentType(ContentType.JSON)
			.body(myBody)
		.when()
			.post()
		.then()
			.statusCode(201);
		
	}
	
//	@Test
	public void getCount() {
		int count = given()
		.when()
			.get("/list")
		.then()
			.extract().path("id.size()");
		
		System.out.println(count);
	}
	
//	@Test
	public void getElementsWithIds() {
		int count = given()
		.when()
			.get("/list")
		.then()
			.extract().path("id.size()");
		
		System.out.println(count);
	}
	
	@Test
	public void getElementsList() {
		Response r1 = given()
		.when()
			.get("/list");
		
//		List<String> s1= from(r1.toString()).getList("");
	}
}
